import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './components/app';
import ErrorBoundry from './components/error-boundry';
import TilestoreService from './services/tilestore-service';
import { TilestoreServiceProvider } from './components/tilestore-service-context';

import store from './store';

const tilestoreService = new TilestoreService();

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundry>
      <TilestoreServiceProvider value={tilestoreService}>
        <Router>
          <App />
        </Router>
      </TilestoreServiceProvider>
    </ErrorBoundry>
  </Provider>,
  document.getElementById('root')
);
