import updateGoodsList from './goods-list';
import updateShoppingCart from './shopping-cart';
import updateGoodsReviewed from './goods-reviewed';

const reducer = (state, action) => {
    return {
        goodsList: updateGoodsList(state, action),
        shoppingCart: updateShoppingCart(state, action),
        goodsReviewed: updateGoodsReviewed(state, action)
    };
};

export default reducer;
