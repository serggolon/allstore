const updateCartItems = (cartItems, item, idx) => {

    if (item.count === 0) {
        return [
            ...cartItems.slice(0, idx),
            ...cartItems.slice(idx + 1)
        ];
    }

    if (idx === -1) {
        return [
            ...cartItems,
            item
        ];
    }

    return [
        ...cartItems.slice(0, idx),
        item,
        ...cartItems.slice(idx + 1)
    ];
};

const updateCartItem = (good, item = {}, quantity) => {

    const {
        id = good.id,
        count = 0,
        title = good.title,
        total = 0
    } = item;

    return {
        id,
        title,
        count: count + quantity,
        total: total + quantity * good.price
    };
};

const updateCounts = (cartItems, newItem, property) => {
    let newPropertyCounter = newItem[property];

    cartItems.map((item) => {
        if (item.id !== newItem.id)
            return newPropertyCounter += item[property];
        return newPropertyCounter += 0;
    });

    return (newPropertyCounter);
};

const updateOrder = (state, goodId, quantity) => {

    const {goodsList: {goods}, shoppingCart: {cartItems}} = state;

    const good = goods.find(({id}) => id === goodId);
    const itemIndex = cartItems.findIndex(({id}) => id === goodId);
    const item = cartItems[itemIndex];
    const newItem = updateCartItem(good, item, quantity);

    return {
        cartItems: updateCartItems(cartItems, newItem, itemIndex),
        orderTotal: updateCounts(cartItems, newItem, 'total'),
        numItems: updateCounts(cartItems, newItem, 'count'),
    };
};

const updateShoppingCart = (state, action) => {

    if (state === undefined) {
        return {
            cartItems: [],
            orderTotal: 0,
            numItems: 0,
        }
    }

    switch (action.type) {
        case 'GOOD_ADDED_TO_CART':
            return updateOrder(state, action.payload, 1);

        case 'GOOD_REMOVED_FROM_CART':
            return updateOrder(state, action.payload, -1);

        case 'ALL_GOODS_REMOVED_FROM_CART':
            const item = state.shoppingCart.cartItems.find(({id}) => id === action.payload);
            return updateOrder(state, action.payload, -item.count);
        default:
            return state.shoppingCart;
    }
};

export default updateShoppingCart;