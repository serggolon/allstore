const updateGoodsList = (state, action) => {

    if (state === undefined) {
        return {
            goods: [],
            loading: true,
            error: null,
        }
    }

    const {goods} = state.goodsList;
    const newGoods = action.payload;


    const updateGoodsArray = (goods, newGoods = []) => {

        if (goods.length === 0 && newGoods.length === 0) {
            return [];
        }

        if (goods.length === 0 && newGoods.length !== 0) {
            return newGoods;
        }

        if (goods.length !== 0) {
            if (newGoods.length !== 0) {
                const newGoodsId = newGoods[0].id;
                const elemExist = goods.find(({id}) => id === newGoodsId);
                if (elemExist === undefined) {
                    return [
                        ...goods,
                        ...newGoods
                    ];
                }
            }
            return [
                ...goods
            ];
        }
    };


    switch (action.type) {
        case 'FETCH_GOODS_SUCCESS':
            return {
                goods: updateGoodsArray(goods, newGoods),
                loading: false,
                error: null
            };

        case 'FETCH_GOODS_REQUEST':
            return {
                goods: goods,
                loading: true,
                error: null
            };

        case 'FETCH_GOODS_FAILURE':
            return {
                goods: goods,
                loading: false,
                error: action.payload
            };
        default:
            return state.goodsList;
    }
};

export default updateGoodsList;