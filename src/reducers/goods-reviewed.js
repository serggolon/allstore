const updateGoodReviewedItems = (state, item) => {

    const {goodsReviewed: {revGoods}} = state;

    if (revGoods.length === 0) {
        return [
            item
        ];
    } else {
        return [
            ...revGoods,
            item
        ];
    }
};

const updateGoodsReviewed = (state, action) => {

    if (state === undefined) {
        return {
            revGoods: [],
            loading: true,
            error: null,
        }
    }

    switch (action.type) {
        case 'FETCH_GOOD_SUCCESS':
            return {
                revGoods: updateGoodReviewedItems(state, action.payload),
                loading: false,
                error: null
            };

        case 'FETCH_GOOD_REQUEST':
            return {
                revGoods: state.goodsReviewed.revGoods,
                loading: true,
                error: null
            };

        case 'FETCH_GOOD_FAILURE':
            return {
                revGoods: state.goodsReviewed.revGoods,
                loading: false,
                error: action.payload
            };

        default:
            return state.goodsReviewed;
    }
};

export default updateGoodsReviewed;
