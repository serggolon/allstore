export default class TilestoreService {

    data = [
        {
            id: '1',
            definition: 'tile',
            collection: 'Collection 1',
            manufacturer: 'Opozno',
            countryOfOrigin: 'Poland',
            titleImage: 'https://st.hzcdn.com/simgs/5bd1131707519279_4-2964/home-design.jpg',
            title: 'tile 1',
            color: 'Белый',
            purpose: 'Напольная',
            surface: 'Матовая',
            dimensions: '300*300',
            unitOfMeasurement: 'мм',
            paymentPer: 'за 1 кв.м.',
            price: 200,

        },
        {
            id: '6',
            definition: 'tile',
            collection: 'Collection 2',
            manufacturer: 'Opozno',
            countryOfOrigin: 'Poland',
            titleImage: 'https://st.hzcdn.com/simgs/5bd1131707519279_4-2964/home-design.jpg',
            title: 'tile 2',
            color: 'Белый',
            purpose: 'Настенная',
            surface: 'Матовая',
            dimensions: '250*300',
            unitOfMeasurement: 'мм',
            paymentPer: 'за 1 кв.м.',
            price: 200,
        },
        {
            id: '7',
            definition: 'tile',
            collection: 'Collection 1',
            manufacturer: 'Opozno',
            countryOfOrigin: 'Poland',
            titleImage: 'https://st.hzcdn.com/simgs/5bd1131707519279_4-2964/home-design.jpg',
            title: 'tile 3',
            color: 'Белый',
            purpose: 'Фриз',
            surface: 'Матовая',
            dimensions: '30*400',
            unitOfMeasurement: 'мм',
            paymentPer: 'за 1 шт.',
            price: 50,
        },
        {
            id: '2',
            definition: 'laminate',
            collection: 'Collection 8',
            title: 'laminate 1',
            manufacturer: 'Cersanite',
            countryOfOrigin: 'Chezh Republik',
            titleImage: 'https://s7d1.scene7.com/is/image/TileShop/gravity_floor_buildout_noplant_KJ_Hov%3AHover_Image_All',
            color: 'Белый',
            surface: 'Матовая',
            dimensions: '30*400',
            unitOfMeasurement: 'мм',
            paymentPer: 'за 1 кв. м.',
            price: 1000,
        },
        {
            id: '3',
            collection: 'Collection 3',
            definition: 'laminate',
            title: 'laminate 2',
            manufacturer: 'Versache',
            countryOfOrigin: 'Poland',
            color: 'Белый',
            purpose: 'Напольная',
            surface: 'Матовая',
            dimensions: '30*30',
            unitOfMeasurement: 'кв.м.',
            price: 200,
            titleImage: 'https://st.hzcdn.com/simgs/5bd1131707519279_4-2964/home-design.jpg',
        },
        {
            id: '4',
            definition: 'laminate',
            collection: 'Collection 4',
            title: 'laminate 3',
            manufacturer: 'Brak',
            countryOfOrigin: 'Poland',
            color: 'Белый',
            purpose: 'Напольная',
            surface: 'Матовая',
            dimensions: '30*30',
            unitOfMeasurement: 'кв.м.',
            price: 200,
            titleImage: 'https://st.hzcdn.com/simgs/5bd1131707519279_4-2964/home-design.jpg',
        },
        {
            id: '5',
            definition: 'tile',
            collection: 'Collection 5',
            title: 'tile 4',
            manufacturer: 'Obama',
            countryOfOrigin: 'Poland',
            color: 'Белый',
            purpose: 'Напольная',
            surface: 'Матовая',
            dimensions: '30*30',
            unitOfMeasurement: 'кв.м.',
            price: 200,
            titleImage: 'https://st.hzcdn.com/simgs/5bd1131707519279_4-2964/home-design.jpg',
        },
    ];

    getGoods() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (Math.random() > 0.95) {
                    reject(new Error('Something bad happend'));
                } else {
                    resolve(this.data);
                }
            }, 700);
        });
    };

    getTileCollections() {
        const resData = () => {
            let allTiles = [];
            this.data.map((item) => {
                if (item.definition === 'tile') {
                    allTiles.push(item);
                }
            });

            let tileCollections = [];
            allTiles.map((item) => {
                let colExist = false;

                tileCollections.map((colItem) => {
                    if (item.collection === colItem.collection) {
                        colExist = true;
                    }
                });

                if (!colExist) {
                    const pushItem = {
                        id: item.id,
                        collection: item.collection,
                        manufacturer: item.manufacturer,
                        countryOfOrigin: item.countryOfOrigin,
                        titleImage: item.titleImage
                    };

                    tileCollections.push(pushItem);
                }

            });

            return tileCollections;
        };

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (Math.random() > 0.95) {
                    reject(new Error('Something bad happend'));
                } else {
                    resolve(resData());
                }
            }, 700);
        });
    };



    getAllTiles() {
        const resData = () => {

            let allTiles = [];
            this.data.map((item) => {
                if (item.definition === 'tile') {
                    allTiles.push(item);
                }
            });
            return allTiles;
        };

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (Math.random() > 0.95) {
                    reject(new Error('Something bad happend'));
                } else {
                    resolve(resData());
                }
            }, 700);
        });
    };

    getAllLaminate() {
        const resData = () => {

            let allLaminate = [];
            this.data.map((item) => {
                if (item.definition === 'laminate') {
                    allLaminate.push(item);
                }
            });
            return allLaminate;
        };

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (Math.random() > 0.95) {
                    reject(new Error('Something bad happend'));
                } else {
                    resolve(resData());
                }
            }, 700);
        });
    };

    getGood(id) {

        let resItem = {};
        this.data.map((item) => {
            if (item.id === id) {
                resItem = item;
            }
        });

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (Math.random() > 0.95) {
                    reject(new Error('Something bad happend'));
                } else {
                    resolve(resItem);
                }
            }, 700);
        });
    };
};
