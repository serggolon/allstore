import React from 'react';
import {TilestoreServiceConsumer} from '../tilestore-service-context';

const withTileStoreService = () => (Wrapped) => {
  return (props) => {
    return (
      <TilestoreServiceConsumer>
        {
          (tilestoreService) => {
            return(
            <Wrapped {...props}
            tilestoreService={tilestoreService}/>);
          }
        }
      </TilestoreServiceConsumer>
    );
  }
}

export default withTileStoreService;
