import React from 'react';
import './goods-list-item.css';

const GoodsListItem = ({good, onCollectionsClick}) => {

    const {
        manufacturer, collection,
        countryOfOrigin, titleImage
    } = good;

    return (
        <div className="tiles-list-item"
             onClick={onCollectionsClick}>
            <img src={titleImage} alt="cover"/>

            <div className="tile-details">
                <div className="tile-collection">{collection}</div>
                <div className="tile-manufacturer">{manufacturer}</div>
                <div className="tile-countryOfOrigin">{countryOfOrigin}</div>
            </div>
        </div>
    );
};

export default GoodsListItem;