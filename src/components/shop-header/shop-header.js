import React from 'react';
import './shop-header.css';
import { Link } from 'react-router-dom';
import {connect} from "react-redux";

const ShopHeader = ({items, total}) => {
    return (
        <header className="shop-header">

            <Link to="/tiles">
                <div className="logo">Керамическая плитка</div>
            </Link>

            <Link to="/laminates">
                <div className="logo">Ламинированные полы</div>
            </Link>

            <Link to="/contacts">
                <div className="logo contacts">Контакты</div>
            </Link>

            <Link to="/cart">
                <div className="shopping-cart">
                    <i className="cart-icon fa fa-shopping-cart" />
                    {items} items (${total})
                </div>
            </Link>

        </header>
    )
};

const mapStateToProps = ({shoppingCart: {numItems, orderTotal}}) => {
    return {
        items: numItems,
        total: orderTotal
    };
};

export default connect(mapStateToProps)(ShopHeader);