import React from 'react';
import {Switch, Route} from 'react-router-dom';
import ShopHeader from '../shop-header';
import {
    HomePage, TilesPage, LaminatesPage,
    CartPage, ContactsPage, GoodDetailsPage
    } from '../pages';
import ShopFooter from '../shop-footer';

import './app.css';

const App = () => {

    return (
        <main className="app">
            <ShopHeader/>
            <Switch>
                <Route
                    path="/"
                    component={HomePage}
                    exact/>

                <Route
                    path="/tiles"
                    component={TilesPage}
                    exact/>

                <Route
                    path="/laminates"
                    component={LaminatesPage}
                    exact/>

                <Route
                    path="/good-details/:id"
                    render={({match}) => {
                        const {id} = match.params;
                        return <GoodDetailsPage goodId={id}/>
                    }}/>

                <Route
                    path="/cart"
                    component={CartPage}
                    exact/>

                <Route
                    path="/contacts"
                    component={ContactsPage}
                    exact/>

                <Route render={() => <h2>Page not found</h2>}/>

            </Switch>
            <ShopFooter/>
        </main>
    );
};

export default App;
