import GoodsListContainer from './goods-list-container';
import GoodsListItemDetailsContainer from './goods-list-item-details-container';

export {
    GoodsListContainer,
    GoodsListItemDetailsContainer
}
