import React, {Component} from 'react';
import {connect} from 'react-redux';
import withTileStoreService from '../hoc';
import {fetchGood, goodLoaded, goodAddedToCart} from "../../actions";
import {compose} from '../../utils';

import GoodsListItemDetails from '../goods-list-item-details';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';

class GoodsListItemDetailsContainer extends Component {

    componentDidMount() {
        const {revGoods, goodId, goods} = this.props;
        const selectedRevItem = revGoods.find(({id}) => id === goodId);

        if (!selectedRevItem || selectedRevItem.length === 0) {
            const selectedItemDownloaded = goods.find(({id}) => id === goodId);
            if (selectedItemDownloaded !== undefined && selectedItemDownloaded.length !== 0) {
                this.props.goodLoaded(selectedItemDownloaded);
            } else {
                this.props.fetchGood();
            }
        }
    };

    renderItem(array, onAddedToCart) {
        return (
            <ul>
                {
                    array.map((item) => {
                        return (
                            <li key={item.id}>
                                <GoodsListItemDetails good={item} onAddedToCart={() => onAddedToCart(item.id)}/>
                            </li>
                        );
                    })
                }
            </ul>
        )
    }


    render() {
        const {goodId, revGoods, goods, loading, error, onAddedToCart} = this.props;
        const itemSelected = goods.find(({id}) => id === goodId);
        let selectedGoods = [];
        if (itemSelected) {
            selectedGoods = goods.filter((item) => item.collection === itemSelected.collection);
            console.log(selectedGoods);
        }

        if (loading) {
            return <Spinner/>;
        }
        if (error) {
            return <ErrorIndicator/>
        }


        if (selectedGoods.length !== 0) {
            return (this.renderItem(selectedGoods, onAddedToCart));
        }

        if (revGoods.length !== 0) {
            return (this.renderItem(revGoods, onAddedToCart));
        }

        return (
            <div> Элемент не выбран </div>
        );

    };
}

const mapStateToProps = ({goodsReviewed: {revGoods, loading, error}, goodsList: {goods}}) => {
    return {revGoods, loading, error, goods};
};

const mapDispatchToProps = (dispatch, {tilestoreService, goodId}) => {
    return {
        fetchGood: fetchGood(tilestoreService, dispatch, goodId),
        goodLoaded: (items) => dispatch(goodLoaded(items)),
        onAddedToCart: (id) => dispatch(goodAddedToCart(id)),
    };
};

export default compose(
    withTileStoreService(),
    connect(mapStateToProps, mapDispatchToProps)
)(GoodsListItemDetailsContainer);