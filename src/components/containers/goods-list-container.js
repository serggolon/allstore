import React, {Component} from 'react';
import {connect} from 'react-redux';
import withTileStoreService from '../hoc';
import {fetchGoods} from "../../actions";
import {compose} from '../../utils';

import GoodsList from '../goods-list';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';

class GoodsListContainer extends Component {

    componentDidMount() {
        this.props.fetchGoods();
    }

    render() {
        const {goods, definition, loading, error, onCollectionsClick} = this.props;

        if (loading) {
            return <Spinner/>;
        }
        if (error) {
            return <ErrorIndicator/>
        }

        const goodsRenderArr = goods.filter((item) => item.definition === definition);
        return <GoodsList goods={goodsRenderArr} onCollectionsClick={onCollectionsClick}/>;
    };
};

const mapStateToProps = ({goodsList: {goods, loading, error}}) => {
    return {goods, loading, error};
};

const mapDispatchToProps = (dispatch, {tilestoreService, definition}) => {

    return {
        fetchGoods: fetchGoods(tilestoreService, dispatch, definition),
    };
};

export default compose(
    withTileStoreService(),
    connect(mapStateToProps, mapDispatchToProps)
)(GoodsListContainer);