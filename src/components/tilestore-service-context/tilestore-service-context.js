import React from 'react';

const {
  Provider: TilestoreServiceProvider,
  Consumer: TilestoreServiceConsumer
} = React.createContext();

export {
  TilestoreServiceProvider,
  TilestoreServiceConsumer
};
