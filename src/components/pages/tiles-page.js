import React from 'react';
import { GoodsListContainer } from '../containers';
import {withRouter} from 'react-router-dom';

import './tiles-page.css';

const TilesPage = ({history}) => {

    return (
        <div className="tiles-page">
            <GoodsListContainer
                definition = {'tile'}
                onCollectionsClick={(goodId) => history.push(`/good-details/${goodId}`)}/>
        </div>
    );

};

export default withRouter(TilesPage);