import React from 'react';
import { GoodsListContainer } from '../containers';
import {withRouter} from 'react-router-dom';

import './laminates-page.css';

const LaminatesPage = ({history}) => {

    return (
        <div className="laminates-page">
            <GoodsListContainer
                definition = {'laminate'}
                onCollectionsClick={(goodId) => history.push(`/good-details/${goodId}`)}/>
        </div>
    );

};

export default withRouter(LaminatesPage);