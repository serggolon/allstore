import React from 'react';
import {GoodsListItemDetailsContainer} from '../containers';


import './good-details-page.css';

const GoodDetailsPage = ({goodId}) => {
    return (
        <GoodsListItemDetailsContainer goodId={goodId}/>
    );
};

export default GoodDetailsPage;