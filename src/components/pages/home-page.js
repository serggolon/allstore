import React from 'react';
import { GoodsListContainer } from '../containers';
import {withRouter} from 'react-router-dom';

import './home-page.css';

const HomePage = ({history}) => {

    return (
        <div className="home-page">
            <GoodsListContainer
                definition = {''}
                onCollectionsClick={(goodId) => history.push(`/good-details/${goodId}`)}/>
        </div>
    );

};

export default withRouter(HomePage);