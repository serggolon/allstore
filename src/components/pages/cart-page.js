import React from 'react';
import ShoppingCartTable from '../shopping-cart-table';

import './cart-page.css';

const CartPage = () => {
    return (
        <section className='cart-page'>
            <ShoppingCartTable/>
        </section>
    );
};

export default CartPage;