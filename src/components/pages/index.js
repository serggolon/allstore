import HomePage from './home-page';
import TilesPage from './tiles-page';
import LaminatesPage from './laminates-page';
import GoodDetailsPage from './good-details-page';
import CartPage from './cart-page';
import ContactsPage from './contacts-page';


export {
    HomePage,
    TilesPage,
    LaminatesPage,
    GoodDetailsPage,
    CartPage,
    ContactsPage
};