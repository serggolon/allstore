import React from 'react';


import './goods-list-item-details.css';

const GoodsListItemDetails = ({good, onAddedToCart}) => {
    const {
        title, price, color,
        purpose, surface, dimensions,
        unitOfMeasurement
    } = good;

    return (
        <div>
            <span className="tile-title"> {title}</span>
            <div className="tile-price">{price}</div>
            <div className="tile-color">{color}</div>
            <div className="tile-purpose">{purpose}</div>
            <div className="tile-surface">{surface}</div>
            <div className="tile-dimensions">{dimensions}</div>
            <div className="tile-unitOfMeasurement">{unitOfMeasurement}</div>
            <button
                onClick={onAddedToCart}
                className="btn btn-info add-to-cart">
                Add to cart
            </button>
        </div>
    );
};

export default GoodsListItemDetails;