import React from 'react';
import GoodsListItem from '../goods-list-item';

import './goods-list.css';

const GoodsList = ({goods=[], onCollectionsClick}) => {
    return (
        <ul className="tiles-list">
            {
                goods.map((good) => {
                    return (
                        <li key={good.id} className='list-item'>
                            <GoodsListItem
                                good={good}
                                onCollectionsClick={() => onCollectionsClick(good.id)}/>

                        </li>
                    );
                })
            }
        </ul>
    );
};

export default GoodsList;

