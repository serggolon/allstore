const goodsLoaded = (newGoods) => {
    return {
        type: 'FETCH_GOODS_SUCCESS',
        payload: newGoods
    };
};

const goodsRequested = () => {
    return {
        type: 'FETCH_GOODS_REQUEST'
    }
};

const goodsError = (error) => {
    return {
        type: 'FETCH_GOODS_FAILURE',
        payload: error
    };
};

const fetchGoods = (tilestoreService, dispatch, definition) => () => {
    dispatch(goodsRequested());

    switch (definition) {

        case 'tile':
            tilestoreService.getAllTiles()
                .then((data) => dispatch(goodsLoaded(data)))
                .catch((err) => dispatch(goodsError(err)));
            break;

        case 'laminate':
            tilestoreService.getAllLaminate()
                .then((data) => dispatch(goodsLoaded(data)))
                .catch((err) => dispatch(goodsError(err)));
            break;

        default:
            tilestoreService.getGoods()
                .then((data) => dispatch(goodsLoaded(data)))
                .catch((err) => dispatch(goodsError(err)));
    };

};

export default fetchGoods;
