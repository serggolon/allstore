const goodAddedToCart = (goodID) => {
    return {
        type: 'GOOD_ADDED_TO_CART',
        payload: goodID
    };
};

const goodRemovedFromCart = (goodID) => {
    return {
        type: 'GOOD_REMOVED_FROM_CART',
        payload: goodID
    };
};

const allGoodsRemovedFromCart = (goodID) => {
    return {
        type: 'ALL_GOODS_REMOVED_FROM_CART',
        payload: goodID
    };
};

export {
    goodAddedToCart,
    goodRemovedFromCart,
    allGoodsRemovedFromCart
};