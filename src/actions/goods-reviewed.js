const goodLoaded = (newGoods) => {
    return {
        type: 'FETCH_GOOD_SUCCESS',
        payload: newGoods
    };
};

const goodRequested = () => {
    return {
        type: 'FETCH_GOOD_REQUEST'
    }
};

const goodError = (error) => {
    return {
        type: 'FETCH_GOOD_FAILURE',
        payload: error
    };
};

const fetchGood = (tilestoreService, dispatch, id) => () => {
    dispatch(goodRequested());
    tilestoreService.getGood(id)
        .then((data) => dispatch(goodLoaded(data)))
        .catch((err) => dispatch(goodError(err)));
};

export {
    goodLoaded,
    fetchGood
}